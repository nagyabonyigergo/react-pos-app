import * as actionTypes from './actionTypes';

export const getTableData = (id) => {
  return {
    type: actionTypes.GET_TABLE_DATA,
    id: id,
  };
};

export const changeTableWaiter = (table, newWaiter) => {
  return {
    type: actionTypes.CHANGE_TABLE_WAITER,
    table: table,
    newWaiter: newWaiter,
  };
};

export const changeTables = (table, newTable) => {
  return {
    type: actionTypes.CHANGE_TABLES,
    table: table,
    newTable: newTable,
  };
};

export const onAddItemToOrders = (label, price, table) => {
  return {
    type: actionTypes.ADD_ITEM_TO_ORDERS,
    label: label,
    price: price,
    table: table,
  };
};

export const deleteOrder = (selectedOrders, table) => {
  return {
    type: actionTypes.DELETE_ORDERS,
    selectedOrders: selectedOrders,
    table: table,
  };
};

export const payOrder = (selectedOrders, table) => {
  return {
    type: actionTypes.PAY_ORDERS,
    selectedOrders: selectedOrders,
    table: table,
  };
};
