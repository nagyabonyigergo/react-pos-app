export {
  getTableData,
  changeTableWaiter,
  changeTables,
  onAddItemToOrders,
  deleteOrder,
  payOrder,
} from './seats';
