import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../../shared/utility';

const initialState = {
  seats: [
    {
      id: 'table1',
      name: 'Table 1',
    },
    {
      id: 'table2',
      name: 'Table 2',
    },
    {
      id: 'table3',
      name: 'Table 3',
    },
    {
      id: 'table4',
      name: 'Table 4',
    },
    {
      id: 'table5',
      name: 'Table 5',
    },
    {
      id: 'table6',
      name: 'Table 6',
    },
    {
      id: 'table7',
      name: 'Table 7',
    },
  ],
  tables: {
    table1: {
      name: 'Table 1',
      orders: [
        { id: 1, label: 'Jelen', price: 25 },
        { id: 2, label: 'Jelen', price: 25 },
      ],
      selectedOrders: [],
      fullPrice: 50,
      waiter: 'Test Elek',
    },
    table2: {
      name: 'Table 2',
      orders: [
        { id: 1, label: 'Jelen', price: 25 },
        { id: 2, label: 'Jelen', price: 25 },
      ],
      selectedOrders: [],
      fullPrice: 0,
      waiter: null,
    },
    table3: {
      name: 'Table 3',
      orders: [],
      selectedOrders: [],
      fullPrice: 0,
      waiter: null,
    },
    table4: {
      name: 'Table 4',
      orders: [],
      selectedOrders: [],
      fullPrice: 0,
      waiter: null,
    },
    table5: {
      name: 'Table 5',
      orders: [],
      selectedOrders: [],
      fullPrice: 0,
      waiter: null,
    },
    table6: {
      name: 'Table 6',
      orders: [],
      selectedOrders: [],
      fullPrice: 0,
      waiter: null,
    },
    table7: {
      name: 'Table 7',
      orders: [],
      selectedOrders: [],
      fullPrice: 0,
      waiter: null,
    },
  },
  waiters: ['Test Elek', 'Pro Pal'],
  selectedTable: null,
};

const changeTableWaiter = (state, action) => {
  const updateWaiter = updateObject(state.tables[action.table], {
    waiter: action.newWaiter,
  });
  const updateTable = updateObject(state.tables, {
    [action.table]: updateWaiter,
  });
  const updateSelectedTable = updateObject(state.selectedTable, updateWaiter);
  const updateState = updateObject(state, {
    tables: updateTable,
    selectedTable: updateSelectedTable,
  });
  return updateObject(state, updateState);
};

const changeTables = (state, action) => {
  const newTable = updateObject(state.tables[action.newTable], {
    waiter: state.tables[action.table].waiter,
    orders: state.tables[action.table].orders,
    fullPrice: state.tables[action.table].fullPrice,
  });
  const oldTable = updateObject(state.tables[action.table], {
    waiter: null,
    orders: [],
    fullPrice: null,
  });
  const updateTables = updateObject(state.tables, {
    [action.table]: oldTable,
    [action.newTable]: newTable,
  });

  return updateObject(state, {
    tables: updateTables,
    selectedTable: oldTable,
  });
};

const onAddItemToOrders = (state, action) => {
  const newOrders = state.tables[action.table].orders;
  newOrders.push({
    id: Math.floor(Math.random() * 1000),
    label: action.label,
    price: action.price,
  });
  const newTable = state.tables[action.table];
  newTable.orders = newOrders;
  newTable.fullPrice = state.tables[action.table].fullPrice + action.price;

  const updateTables = updateObject(state.tables, {
    [action.table]: newTable,
  });

  return updateObject(state, {
    tables: updateTables,
  });
};

const deleteOrder = (state, action) => {
  /*  let newOrders = []; */
  let newOrders = [];
  let newPrice = state.tables[action.table].fullPrice;

  if (
    action.selectedOrders.length === state.tables[action.table].orders.length
  ) {
    newPrice = 0;
  } else {
    action.selectedOrders.forEach((order) => {
      state.tables[action.table].orders.forEach((item) => {
        if (item.id !== order && state.tables[action.table].orders.length > 1) {
          newPrice = newPrice - item.price;
          newOrders.push(item);
        } else if (
          state.tables[action.table].orders.length === 1 &&
          item.id === order
        ) {
          newPrice = newPrice - item.price;
          newOrders = [];
        }
      });
    });
  }

  const newTable = state.tables[action.table];
  newTable.orders = newOrders;
  newTable.fullPrice = newPrice;

  const updateTables = updateObject(state.tables, {
    [action.table]: newTable,
  });

  return updateObject(state, {
    tables: updateTables,
  });
};

const getTablesData = (state, action) => {
  return updateObject(state, { selectedTable: state.tables[action.id] });
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.GET_TABLE_DATA:
      return getTablesData(state, action);
    case actionTypes.CHANGE_TABLE_WAITER:
      return changeTableWaiter(state, action);
    case actionTypes.CHANGE_TABLES:
      return changeTables(state, action);
    case actionTypes.ADD_ITEM_TO_ORDERS:
      return onAddItemToOrders(state, action);
    case actionTypes.DELETE_ORDERS:
      return deleteOrder(state, action);
    default:
      return initialState;
  }
};

export default reducer;
