const initialState = {
  foods: [
    {
      label: 'Food 1',
      price: 16,
    },
    {
      label: 'Food 2',
      price: 10,
    },
    {
      label: 'Food 3',
      price: 5,
    },
    {
      label: 'Food 4',
      price: 100,
    },
  ],
  drinks: [
    {
      label: 'Drink 1',
      price: 16,
    },
    {
      label: 'Drink 2',
      price: 10,
    },
    {
      label: 'Drink 3',
      price: 5,
    },
    {
      label: 'Drink 4',
      price: 100,
    },
  ],
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    default:
      return initialState;
  }
};

export default reducer;
