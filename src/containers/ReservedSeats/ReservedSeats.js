import { Table, TableContainer, Toolbar, Typography } from '@material-ui/core';
import React, { Component } from 'react';
import CustomTableBody from '../../components/TableComponents/CustomTableBody/CustomTableBody';
import TableHeader from '../../components/TableComponents/TableHeader/TableHeader';
import './ReservedSeats.scss';

class ReservedSeats extends Component {
  state = {
    header: [
      { id: 'table', label: 'Table' },
      { id: 'name', label: 'Name' },
      { id: 'date', label: 'Date' },
      { id: 'from', label: 'From' },
      { id: 'to', label: 'To' },
    ],
    rows: [
      {
        table: 'Table 1',
        name: 'Gergo',
        date: '2020-02-13',
        from: '20:20',
        to: '21:00',
      },
      {
        table: 'Table 1',
        name: 'PRoba',
        date: '2020-02-13',
        from: '20:20',
        to: '21:00',
      },
      {
        table: 'Table 1',
        name: 'Tamas',
        date: '2020-02-13',
        from: '20:20',
        to: '21:00',
      },
      {
        table: 'Table 1',
        name: 'Info',
        date: '2020-02-13',
        from: '20:20',
        to: '21:00',
      },
      {
        table: 'Table 1',
        name: 'Miki',
        date: '2020-02-13',
        from: '20:20',
        to: '21:00',
      },
      {
        table: 'Table 1',
        name: 'Grog',
        date: '2020-02-13',
        from: '20:20',
        to: '21:00',
      },
      {
        table: 'Table 2',
        name: 'Grig',
        date: '2020-02-13',
        from: '20:20',
        to: '21:00',
      },
    ],
    selected: [],
    open: false,
  };

  handleSelectAllClick = (event) => {
    if (event.target.checked) {
      const newSelected = this.state.rows.map((row) => row.name);
      this.setState({ selected: newSelected });
    } else {
      this.setState({ selected: [] });
    }
  };

  handleSelectOneClick = (name) => {
    const selectedIndex = this.state.selected.indexOf(name);
    let selected = [];

    if (selectedIndex === -1) {
      selected = selected.concat(this.state.selected, name);
    } else if (selectedIndex === 0) {
      selected = selected.concat(this.state.selected.slice(1));
    } else if (selectedIndex === this.state.selected.length - 1) {
      selected = selected.concat(this.state.selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      selected = selected.concat(
        this.state.selected.slice(0, selectedIndex),
        this.state.selected.slice(selectedIndex + 1)
      );
    }

    this.setState({ selected });
  };

  handleDeleteSelectedItems = () => {
    let rows = [];
    this.state.rows.forEach((row) => {
      if (!this.state.selected.includes(row.name)) {
        rows.push(row);
      }
    });
    this.setState({ rows, selected: [] });
  };

  render() {
    let collapsibleHeaders = this.state.header.slice(2);
    return (
      <div>
        <TableContainer classes={{ root: 'res-table-container' }}>
          <Toolbar>
            {this.state.selected.length === 0 ? (
              <Typography
                variant='h6'
                component='div'
                style={{ width: '100%' }}
                align='center'
              >
                Reserved Seats
              </Typography>
            ) : (
              <div className='res-delete-toolbar'>
                <h3>{this.state.selected.length} selected</h3>
                <button onClick={this.handleDeleteSelectedItems}>Delete</button>
              </div>
            )}
          </Toolbar>
          <Table style={{ width: '100%', marginTop: 40 }}>
            <TableHeader
              onSelectAllClick={this.handleSelectAllClick}
              headers={this.state.header}
            ></TableHeader>
            <CustomTableBody
              selected={this.state.selected}
              onClickSelect={this.handleSelectOneClick}
              collapsibleHeaders={collapsibleHeaders}
              toCollapse={true}
              rows={this.state.rows}
            ></CustomTableBody>
          </Table>
        </TableContainer>
      </div>
    );
  }
}

export default ReservedSeats;
