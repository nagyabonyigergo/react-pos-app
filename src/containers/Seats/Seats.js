import React, { Component } from 'react';
import { connect } from 'react-redux';
import ReservedSeatCards from '../../components/SeatCards/ReservedSeatCards/ReservedSeatCards';
import SeatCards from '../../components/SeatCards/SeatsCards/SeatCards';
import './Seats.scss';

class Seats extends Component {
  seatCardClickHandler = (id) => {
    this.props.history.push('/tables/' + id);
  };
  reservedSeatCardClickHandler = () => {
    this.props.history.push('/res');
  };

  render() {
    return (
      <div className='seats-flex'>
        <ReservedSeatCards
          reservedSeatCardClickHandler={this.reservedSeatCardClickHandler}
        />
        <SeatCards
          onClick={this.seatCardClickHandler}
          seats={this.props.seats}
        />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    seats: state.seats.seats,
  };
};

export default connect(mapStateToProps)(Seats);
