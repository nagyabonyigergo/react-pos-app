import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../store/actions/index';
import SeatInfoMobile from '../../components/SeatInfo/SeatInfoMobile/SeatInfoMobile';
import SeatInfoDesktop from '../../components/SeatInfo/SeatInfoDesktop/SeatInfoDesktop';

class SeatInfo extends Component {
  state = {
    tabValue: 0,
    header: [
      { id: 'Name', label: 'Name' },
      { id: 'price', label: 'Price' },
    ],
    isOpen: false,
    dialogItems: null,
    title: null,
    selectedValue: null,
    function: null,
    menuItems: null,
    selectedOrders: [],
  };

  componentWillMount() {
    const id = this.props.match.params.id;
    this.seatInfo = this.props.getTableData(id);
  }

  handleTabChange = (event, newValue) => {
    this.setState({ tabValue: newValue });
  };

  handleTableChangeDialogClick = () => {
    let tableNames = this.props.tableNames.filter(
      (table) =>
        table !== this.props.seatInfo.name.toLowerCase().replace(/\s/g, '')
    );
    this.setState({
      isOpen: true,
      dialogItems: tableNames,
      title: 'Table Change',
      function: this.handleDialogTableClick,
    });
  };

  handleWaiterChangeDialogClick = () => {
    let waiters = this.props.waiters.filter(
      (waiter) => waiter !== this.props.seatInfo.waiter
    );
    this.setState({
      isOpen: true,
      dialogItems: waiters,
      title: 'Waiter Change',
      function: this.handleDialogWaiterClick,
    });
  };

  handleDialogClose = () => {
    this.setState({ isOpen: false });
  };

  handleDialogWaiterClick = (newWaiter) => {
    this.setState({ isOpen: false });
    let table = this.props.seatInfo.name.toLowerCase().replace(/\s/g, '');
    this.props.setNewTableWaiter(table, newWaiter);
  };

  handleDialogTableClick = (newTable) => {
    this.setState({ isOpen: false });
    let table = this.props.seatInfo.name.toLowerCase().replace(/\s/g, '');
    this.props.onChangeTables(table, newTable);
  };

  handleOnLoadMenuItemsType = (type) => {
    if (type === 'food') {
      this.setState({ menuItems: this.props.foods });
    } else if (type === 'drink') {
      this.setState({ menuItems: this.props.drinks });
    }
  };

  handleMenuItemClick = (itemName, itemPrice) => {
    let table = this.props.seatInfo.name.toLowerCase().replace(/\s/g, '');
    this.props.onAddItemToOrders(itemName, itemPrice, table);
  };

  handleSelectAllClick = (event) => {
    if (event.target.checked) {
      const newSelected = this.props.seatInfo.orders.map((row) => row.id);
      this.setState({ selectedOrders: newSelected });
    } else {
      this.setState({ selectedOrders: [] });
    }
  };

  handleDeleteOrderClick = () => {
    let table = this.props.seatInfo.name.toLowerCase().replace(/\s/g, '');
    this.props.onDeleteOrder(this.state.selectedOrders, table);
    this.setState({ selectedOrders: [] });
  };

  handleSelectOneClick = (name) => {
    const selectedIndex = this.state.selectedOrders.indexOf(name);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(this.state.selectedOrders, name);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(this.state.selectedOrders.slice(1));
    } else if (selectedIndex === this.state.selectedOrders.length - 1) {
      newSelected = newSelected.concat(this.state.selectedOrders.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        this.state.selectedOrders.slice(0, selectedIndex),
        this.state.selectedOrders.slice(selectedIndex + 1)
      );
    }

    this.setState({ selectedOrders: newSelected });
  };

  render() {
    let render;
    if (this.props.seatInfo && window.innerWidth < 960) {
      render = (
        <SeatInfoMobile
          tabValue={this.state.tabValue}
          header={this.state.header}
          seatInfo={this.props.seatInfo}
          title={this.state.title}
          dialogItems={this.state.dialogItems}
          function={this.state.function}
          menuItems={this.state.menuItems}
          isOpen={this.state.isOpen}
          selectedOrders={this.state.selectedOrders}
          handleTabChange={this.handleTabChange}
          handleSelectAllClick={this.handleSelectAllClick}
          handleSelectOneClick={this.handleSelectOneClick}
          handleMenuItemClick={this.handleMenuItemClick}
          handleOnLoadMenuItemsType={this.handleOnLoadMenuItemsType}
          handleTableChangeDialogClick={this.handleTableChangeDialogClick}
          handleDialogClose={this.handleDialogClose}
          handleDeleteOrderClick={this.handleDeleteOrderClick}
        />
      );
    } else if (this.props.seatInfo && window.innerWidth >= 960) {
      render = (
        <SeatInfoDesktop
          header={this.state.header}
          seatInfo={this.props.seatInfo}
          title={this.state.title}
          dialogItems={this.state.dialogItems}
          function={this.state.function}
          menuItems={this.state.menuItems}
          selectedOrders={this.state.selectedOrders}
          isOpen={this.state.isOpen}
          handleDialogClose={this.handleDialogClose}
          handleDeleteOrderClick={this.handleDeleteOrderClick}
          handleMenuItemClick={this.handleMenuItemClick}
          handleSelectAllClick={this.handleSelectAllClick}
          handleSelectOneClick={this.handleSelectOneClick}
          handleOnLoadMenuItemsType={this.handleOnLoadMenuItemsType}
          handleTableChangeDialogClick={this.handleTableChangeDialogClick}
        />
      );
    }
    return <div className='seat-info-container'>{render}</div>;
  }
}

const mapStateToProps = (state) => {
  return {
    seatInfo: state.seats.selectedTable,
    waiters: state.seats.waiters,
    tableNames: Object.keys(state.seats.tables),
    drinks: state.menu.drinks,
    foods: state.menu.foods,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getTableData: (id) => dispatch(actions.getTableData(id)),
    setNewTableWaiter: (table, newWaiter) =>
      dispatch(actions.changeTableWaiter(table, newWaiter)),
    onChangeTables: (table, newTable) =>
      dispatch(actions.changeTables(table, newTable)),
    onAddItemToOrders: (itemName, itemPrice, table) =>
      dispatch(actions.onAddItemToOrders(itemName, itemPrice, table)),
    onDeleteOrder: (selectedOrders, table) =>
      dispatch(actions.deleteOrder(selectedOrders, table)),
    onPayOrder: (selectedOrders, table) =>
      dispatch(actions.payOrder(selectedOrders, table)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SeatInfo);
