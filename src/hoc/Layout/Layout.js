import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import Toolbar from '../../components/Navigation/Toolbar/Toolbar';
import './Layout.scss';

class Layout extends Component {
  state = {
    showSideDrawer: false,
  };

  sideDrawerToggleHandler = (toShow) => {
    this.setState({ showSideDrawer: toShow });
  };

  onLinkClickHandler = (link) => {
    this.setState({ showSideDrawer: false });
    this.props.history.push(link);
  };

  render() {
    return (
      <div className='container'>
        <Toolbar
          isOpen={this.state.showSideDrawer}
          drawerToggleClicked={this.sideDrawerToggleHandler}
          onClick={this.onLinkClickHandler}
        />
        <div className='content'>{this.props.children}</div>
      </div>
    );
  }
}

export default withRouter(Layout);
