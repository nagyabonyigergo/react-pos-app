import React, { Suspense } from 'react';
import { Redirect, Route, Switch, withRouter } from 'react-router-dom';
import './App.scss';
/* import ReservedSeats from './containers/ReservedSeats/ReservedSeats'; */
import Seats from './containers/Seats/Seats';
import Layout from './hoc/Layout/Layout';

const ReservedSeats = React.lazy(() =>
  import('./containers/ReservedSeats/ReservedSeats')
);

const SeatInfo = React.lazy(() => import('./containers/SeatInfo/SeatInfo'));

let routes = (
  <Suspense fallback={<div>LOading...</div>}>
    <Switch>
      <Route
        path='/res'
        exact
        render={(props) => <ReservedSeats {...props} />}
      />
      <Route path='/tables' exact component={Seats} />
      <Route path='/tables/:id' render={(props) => <SeatInfo {...props} />} />
      <Redirect to='/tables' />
    </Switch>
  </Suspense>
);

function App() {
  return <Layout>{routes}</Layout>;
}

export default withRouter(App);
