import React from 'react';
import Drawer from '../Drawer/Drawer';
import NavigationItems from '../NavigationItems/NavigationItems';
import './Toolbar.scss';

const toolbar = (props) => (
  <header className='toolbar'>
    <Drawer
      isOpen={props.isOpen}
      drawerToggleClicked={props.drawerToggleClicked}
      onClick={props.onClick}
    />
    <nav className='desktop-only'>
      <NavigationItems onClick={props.onClick} />
    </nav>
  </header>
);
export default toolbar;
