import { Button, Drawer, Icon } from '@material-ui/core';
import React from 'react';
import NavigationItems from '../NavigationItems/NavigationItems';
import './Drawer.scss';

const drawer = (props) => {
  return (
    <div className='drawer'>
      <Button onClick={() => props.drawerToggleClicked(true)}>
        <Icon
          style={{
            fontSize: 30,
            marginRight: 10,
            marginLeft: 10,
            color: 'white',
          }}
        >
          menu
        </Icon>
      </Button>
      <Drawer
        anchor='left'
        open={props.isOpen}
        onClose={() => props.drawerToggleClicked(false)}
      >
        <NavigationItems onClick={props.onClick} />
      </Drawer>
    </div>
  );
};

export default drawer;
