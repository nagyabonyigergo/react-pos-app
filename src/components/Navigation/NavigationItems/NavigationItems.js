import React from 'react';
import NavigationItem from './NavigationItem/NavigationItem';
import './NavigationItems.scss';

const navigationItems = (props) => (
  <div className='navigationItems'>
    <div className='main-nav-items'>
      <NavigationItem onClick={() => props.onClick('/tables')}>
        Tables
      </NavigationItem>
      <NavigationItem onClick={() => props.onClick('/res')}>
        Reserved Tables
      </NavigationItem>
    </div>
  </div>
);

export default navigationItems;
