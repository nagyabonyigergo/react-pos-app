import React from 'react';
import './NavigationItem.scss';

const navigationItem = (props) => (
  <div className='nav-item' onClick={props.onClick}>
    <h3>{props.children}</h3>
  </div>
);

export default navigationItem;
