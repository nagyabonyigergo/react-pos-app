import React from 'react';
import './ReservedSeatCard.scss';

const reservedSeatCard = (props) => (
  <div
    className='reserved-seat-card'
    onClick={props.reservedSeatCardClickHandler}
  >
    <div className='res-seat-card-row'>
      <h4 className='res-seat-card-column'>seat 1</h4>
      <h4 className='res-seat-card-column float-right'>19:20-20:40</h4>
    </div>
    <div className='res-seat-card-row'>
      <h4 className='res-seat-card-name res-seat-card-double-column '>
        Test Name Holder
      </h4>
    </div>
  </div>
);

export default reservedSeatCard;
