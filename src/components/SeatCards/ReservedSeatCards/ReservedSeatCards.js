import React, { Component } from 'react';
import ReservedSeatCard from './ReservedSeatCard/ReservedSeatCard';
import './ReservedSeatCards.scss';

class ReservedSeatCards extends Component {
  render() {
    return (
      <div className='reserved-seat-cards'>
        <h2> Reserved </h2>
        <div className='res-seat-cards-container'>
          <ReservedSeatCard
            reservedSeatCardClickHandler={
              this.props.reservedSeatCardClickHandler
            }
          />
          <ReservedSeatCard
            reservedSeatCardClickHandler={
              this.props.reservedSeatCardClickHandler
            }
          />
          <ReservedSeatCard
            reservedSeatCardClickHandler={
              this.props.reservedSeatCardClickHandler
            }
          />
          <ReservedSeatCard
            reservedSeatCardClickHandler={
              this.props.reservedSeatCardClickHandler
            }
          />
        </div>
        <h4
          className='load-more'
          onClick={this.props.reservedSeatCardClickHandler}
        >
          Load more...
        </h4>
      </div>
    );
  }
}

export default ReservedSeatCards;
