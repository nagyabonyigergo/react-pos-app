import React, { Component } from 'react';
import SeatCard from './SeatCard/SeatCard';
import './SeatCards.scss';

class SeatCards extends Component {
  render() {
    let seats = this.props.seats.map((seat) => (
      <SeatCard
        onClick={this.props.onClick}
        key={seat.name}
        id={seat.id}
        name={seat.name}
      />
    ));

    return (
      <div className='seat-cards'>
        <h2> Seats </h2>
        <div className='seat-cards-container'>{seats}</div>
      </div>
    );
  }
}

export default SeatCards;
