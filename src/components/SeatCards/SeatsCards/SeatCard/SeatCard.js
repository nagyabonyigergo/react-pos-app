import React from 'react';
import './SeatCard.scss';

const seatCard = (props) => (
  <div className='seat-card' onClick={() => props.onClick(props.id)}>
    <div className='seat-data'>
      <h2>{props.name}</h2>
    </div>
  </div>
);

export default seatCard;
