import {
  Icon,
  Paper,
  Tabs,
  Tab,
  TableContainer,
  Table,
} from '@material-ui/core';
import React from 'react';
import Dialog from '../../Dialog/Dialog';
import MenuCard from '../../MenuCard/MenuCard';
import CustomTableBody from '../../TableComponents/CustomTableBody/CustomTableBody';
import TableHeader from '../../TableComponents/TableHeader/TableHeader';
import './SeatInfoMobile.scss';

const seatInfoMobile = (props) => (
  <div className='seat-info-mobile-container'>
    <div className='seat-info-header'>
      <h3>{props.seatInfo.name} </h3>
      <h3 className='waiter'>
        <Icon
          style={{
            fontSize: 35,
            marginRight: 5,
            color: '#004445',
          }}
        >
          person
        </Icon>{' '}
        {props.seatInfo.waiter || 'no waiter'}{' '}
      </h3>
    </div>
    <div className='seat-info-body'>
      <div className='seat-info-tabs-container'>
        <Paper>
          <Tabs
            onChange={props.handleTabChange}
            style={{ color: '#004445' }}
            variant='fullWidth'
            aria-label='full width tabs example'
            value={props.tabValue}
          >
            <Tab label='Receipt' />
            <Tab label='Menu Card' />
          </Tabs>
        </Paper>
        <div className='tab-panel' hidden={props.tabValue !== 0}>
          {props.tabValue === 0 && (
            <div
              style={{
                display: 'flex',
                flexDirection: 'column',
                height: '100%',
              }}
            >
              <TableContainer
                classes={{ root: 'seat-info-desktop-table-container' }}
              >
                <Table>
                  <TableHeader
                    headers={props.header}
                    onSelectAllClick={props.handleSelectAllClick}
                  ></TableHeader>
                  <CustomTableBody
                    selected={props.selectedOrders}
                    onClickSelect={props.handleSelectOneClick}
                    collapsibleHeaders={[]}
                    toCollapse={false}
                    rows={props.seatInfo.orders}
                  ></CustomTableBody>
                </Table>
              </TableContainer>
              <div
                className='seat-info-desktop-price'
                style={{
                  boxSizing: 'border-box',
                  paddingLeft: 16,
                  paddingRight: 16,
                }}
              >
                <h3>Total price:</h3>
                <h3> {props.seatInfo.fullPrice}$</h3>
              </div>
            </div>
          )}
        </div>

        <div className='tab-panel' hidden={props.tabValue !== 1}>
          {props.tabValue === 1 && (
            <MenuCard
              onMenuItemClick={props.handleMenuItemClick}
              menuItems={props.menuItems}
              handleOnLoadMenuItemsType={props.handleOnLoadMenuItemsType}
            />
          )}
        </div>
      </div>
      {props.selectedOrders.length === 0 ? (
        <div className='seat-info-table-infos'>
          <button
            className='seat-info-buttons'
            onClick={props.handleTableChangeDialogClick}
          >
            Change Table
          </button>
          <button
            className='seat-info-buttons'
            onClick={props.handleWaiterChangeDialogClick}
          >
            Change Waiter
          </button>
          <Dialog
            title={props.title}
            dialogItems={props.dialogItems || null}
            onItemSelected={props.function}
            onClose={props.handleDialogClose}
            isOpen={props.isOpen}
          />
        </div>
      ) : (
        <div className='seat-info-table-infos'>
          <button
            className='seat-info-buttons-delete'
            onClick={props.handleDeleteOrderClick}
          >
            Delete Item
          </button>
          <button
            className='seat-info-buttons'
            onClick={props.handleDeleteOrderClick}
          >
            Pay
          </button>
        </div>
      )}
    </div>
  </div>
);

export default seatInfoMobile;
