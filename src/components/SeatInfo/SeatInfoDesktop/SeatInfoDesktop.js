import { TableContainer, Icon, Table } from '@material-ui/core';
import React from 'react';
import Dialog from '../../Dialog/Dialog';
import MenuCard from '../../MenuCard/MenuCard';
import CustomTableBody from '../../TableComponents/CustomTableBody/CustomTableBody';
import TableHeader from '../../TableComponents/TableHeader/TableHeader';
import './SeatInfoDesktop.scss';

const seatInfoDesktop = (props) => (
  <div className='seat-info-desktop-container'>
    <div className='seat-info-menu-card'>
      <h3 className='seat-info-menu-header'>Menu</h3>
      <MenuCard
        onMenuItemClick={props.handleMenuItemClick}
        menuItems={props.menuItems}
        handleOnLoadMenuItemsType={props.handleOnLoadMenuItemsType}
      />
    </div>
    <div className='seat-info-desktop-order-details'>
      <div className='seat-info-header'>
        <h3>{props.seatInfo.name} </h3>
        <h3 className='waiter'>
          <Icon
            style={{
              fontSize: 35,
              marginRight: 5,
              color: '#004445',
            }}
          >
            person
          </Icon>{' '}
          {props.seatInfo.waiter || 'no waiter'}{' '}
        </h3>
      </div>
      <TableContainer classes={{ root: 'seat-info-desktop-table-container' }}>
        <Table>
          <TableHeader
            headers={props.header}
            onSelectAllClick={props.handleSelectAllClick}
          ></TableHeader>
          <CustomTableBody
            selected={props.selectedOrders}
            onClickSelect={props.handleSelectOneClick}
            collapsibleHeaders={[]}
            toCollapse={false}
            rows={props.seatInfo.orders}
          ></CustomTableBody>
        </Table>
      </TableContainer>
      <div className='seat-info-desktop-price'>
        <h3>Total price:</h3>
        <h3> {props.seatInfo.fullPrice}$</h3>
      </div>
      {props.selectedOrders.length === 0 ? (
        <div className='seat-info-table-infos'>
          <button
            className='seat-info-buttons'
            onClick={props.handleTableChangeDialogClick}
          >
            Change Table
          </button>
          <button
            className='seat-info-buttons'
            onClick={props.handleWaiterChangeDialogClick}
          >
            Change Waiter
          </button>
          <Dialog
            title={props.title}
            dialogItems={props.dialogItems || null}
            onItemSelected={props.function}
            onClose={props.handleDialogClose}
            isOpen={props.isOpen}
          />
        </div>
      ) : (
        <div className='seat-info-table-infos'>
          <button
            className='seat-info-buttons-delete'
            onClick={props.handleDeleteOrderClick}
          >
            Delete Item
          </button>
          <button
            className='seat-info-buttons'
            onClick={props.handleDeleteOrderClick}
          >
            Pay
          </button>
        </div>
      )}
    </div>
  </div>
);

export default seatInfoDesktop;
