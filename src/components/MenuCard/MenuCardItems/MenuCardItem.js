import React from 'react';
import './MenuCardItem.scss';

const menuCardItem = (props) => (
  <div
    className='menu-card-item'
    onClick={() => props.onClick(props.label, props.price)}
  >
    <h3>{props.label} </h3>
  </div>
);

export default menuCardItem;
