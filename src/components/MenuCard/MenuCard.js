import { Divider } from '@material-ui/core';
import React from 'react';
import './MenuCard.scss';
import MenuCardHeader from './MenuCardHeader/MenuCardHeader';
import MenuCardItem from './MenuCardItems/MenuCardItem';

const menuCard = (props) => (
  <div className='menu-card-container'>
    <MenuCardHeader onClick={props.handleOnLoadMenuItemsType} />
    <Divider classes={{ root: 'menu-card-divider' }} />
    <div className='menu-card-items'>
      {props.menuItems &&
        props.menuItems.map((menuItem) => (
          <MenuCardItem
            onClick={props.onMenuItemClick}
            label={menuItem.label}
            price={menuItem.price}
          />
        ))}
    </div>
  </div>
);
export default menuCard;
