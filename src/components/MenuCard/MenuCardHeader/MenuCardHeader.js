import React from 'react';
import './MenuCardHeader.scss';

const menuCardHeader = (props) => (
  <div className='menu-card-header-container'>
    <button
      className='menu-card-header-button'
      onClick={() => props.onClick('food')}
    >
      Foods
    </button>
    <button
      className='menu-card-header-button'
      onClick={() => props.onClick('drink')}
    >
      Drinks
    </button>
  </div>
);

export default menuCardHeader;
