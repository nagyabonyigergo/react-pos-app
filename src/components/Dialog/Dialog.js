import { Dialog, DialogTitle } from '@material-ui/core';
import React from 'react';

import './Dialog.scss';

const dialog = (props) => {
  let dialogItems;
  if (props.dialogItems) {
    dialogItems = props.dialogItems.map((item) => (
      <h3 className='dialog-item' onClick={() => props.onItemSelected(item)}>
        {item}
      </h3>
    ));
  }
  return (
    <Dialog
      open={props.isOpen}
      onClose={props.onClose}
      classes={{ paper: 'dialog' }}
    >
      <DialogTitle classes={{ root: 'dialog-title' }} id='simple-dialog-title'>
        {props.title}
      </DialogTitle>
      <div className='dialog-content-container'>{dialogItems}</div>
      <button onClick={props.onClose} className='dialog-close-button'>
        Close
      </button>
    </Dialog>
  );
};

export default dialog;
