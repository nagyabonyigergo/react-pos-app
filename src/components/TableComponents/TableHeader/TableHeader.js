import {
  Checkbox,
  Hidden,
  TableCell,
  TableHead,
  TableRow,
} from '@material-ui/core';
import React from 'react';
import './TableHeader.scss';

const tableHeader = (props) => {
  let headerCells = null;
  let counter = 0;

  headerCells = props.headers.map((header) => {
    if (counter > 1) {
      counter++;
      return (
        <Hidden key={counter} smDown>
          <TableCell style={{ color: '#004445' }} key={header.id}>
            {header.label}
          </TableCell>
        </Hidden>
      );
    } else {
      counter++;
      return (
        <TableCell style={{ color: '#004445' }} key={header.id}>
          {header.label}
        </TableCell>
      );
    }
  });

  return (
    <TableHead>
      <TableRow>
        <TableCell padding='checkbox'>
          <Checkbox
            style={{ color: '#004445' }}
            onChange={props.onSelectAllClick}
          />
        </TableCell>
        {headerCells}
      </TableRow>
    </TableHead>
  );
};

export default tableHeader;
