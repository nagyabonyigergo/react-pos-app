import {
  TableCell,
  TableRow,
  Checkbox,
  Table,
  TableHead,
  Collapse,
  Typography,
  TableBody,
} from '@material-ui/core';
import React, { useState } from 'react';

const collapsibleTableRow = (props) => {
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const [isOpen, setIsOpen] = useState(false);

  let onTableRowClick = () => {
    setIsOpen(!isOpen);
  };

  let collapsibleHeaders = props.collapsibleHeaders.map((header) => (
    <TableCell key={header.id} style={{ color: '#004445', border: 'none' }}>
      {header.label}
    </TableCell>
  ));
  let rowCells = Object.values(props.row);
  let mainRowCells;
  let secondaryRowCells = rowCells.slice(2);

  if (props.row.hasOwnProperty('id')) {
    mainRowCells = rowCells.slice(1);
  } else {
    mainRowCells = rowCells.slice(0, 2);
  }

  mainRowCells = mainRowCells.map((cell) => (
    <TableCell
      key={cell}
      onClick={onTableRowClick}
      style={{ color: '#004445', border: 'none' }}
    >
      {cell}
    </TableCell>
  ));

  secondaryRowCells = secondaryRowCells.map((cell) => (
    <TableCell
      key={cell}
      onClick={onTableRowClick}
      style={{ color: '#004445', border: 'none' }}
    >
      {cell}
    </TableCell>
  ));

  return (
    <React.Fragment>
      <TableRow style={{ borderBottom: 'unset' }}>
        <TableCell style={{ border: 'none' }} padding='checkbox'>
          <Checkbox
            style={{ color: '#004445' }}
            onClick={() => props.onClickSelect(props.row.name || props.row.id)}
            checked={props.isSelected}
          />
        </TableCell>
        {mainRowCells}
      </TableRow>
      {props.toCollapse && (
        <TableRow onClick={onTableRowClick}>
          <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
            <Collapse in={isOpen} timeout='auto' unmountOnExit>
              <Typography
                variant='h3'
                component='div'
                style={{ width: '100%', fontSize: 16, margin: 5 }}
                align='left'
              >
                Details
              </Typography>
              <Table size='small'>
                <TableHead>
                  <TableRow>{collapsibleHeaders}</TableRow>
                </TableHead>
                <TableBody>
                  <TableRow>{secondaryRowCells}</TableRow>
                </TableBody>
              </Table>
            </Collapse>
          </TableCell>
        </TableRow>
      )}
    </React.Fragment>
  );
};

export default collapsibleTableRow;
