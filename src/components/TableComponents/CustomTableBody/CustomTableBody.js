import { TableBody } from '@material-ui/core';
import React, { Component } from 'react';
import CollapsibleTableRow from './CollapsibleTableRow/CollapsibleTableRow';
import TableRow from './TableRow/TableRow';

class CustomTableBody extends Component {
  state = {
    isOpen: false,
  };

  onTableRowClick = () => {
    this.setState({ isOpen: !this.state.isOpen });
  };

  checkIsSelected = (name) => {
    return this.props.selected.indexOf(name) !== -1;
  };

  render() {
    let rows;
    if (window.innerWidth >= 960) {
      rows = this.props.rows.map((row) => (
        <TableRow
          key={row.name || row.id}
          row={row}
          isSelected={this.checkIsSelected(row.name || row.id)}
          onClickSelect={this.props.onClickSelect}
        />
      ));
    } else if (window.innerWidth < 960) {
      rows = this.props.rows.map((row) => (
        <CollapsibleTableRow
          key={row.name}
          toCollapse={this.props.toCollapse}
          collapsibleHeaders={this.props.collapsibleHeaders}
          row={row}
          isSelected={this.checkIsSelected(row.name || row.id)}
          onClickSelect={this.props.onClickSelect}
        />
      ));
    }

    return <TableBody>{rows}</TableBody>;
  }
}

export default CustomTableBody;
