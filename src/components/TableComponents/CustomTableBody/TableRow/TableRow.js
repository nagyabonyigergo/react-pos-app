import { TableCell, TableRow, Checkbox } from '@material-ui/core';
import React from 'react';

const tableRow = (props) => {
  let cells;
  if (props.row.hasOwnProperty('id')) {
    cells = Object.values(props.row);
    cells = cells.slice(1);
  } else {
    cells = Object.values(props.row);
  }

  return (
    <TableRow>
      <TableCell padding='checkbox' style={{ color: '#004445' }}>
        <Checkbox
          style={{ color: '#004445' }}
          onClick={() => props.onClickSelect(props.row.name || props.row.id)}
          checked={props.isSelected}
        />
      </TableCell>
      {cells.map((cell) => (
        <TableCell key={cell} style={{ color: '#004445' }}>
          {cell}
        </TableCell>
      ))}
    </TableRow>
  );
};

export default tableRow;
